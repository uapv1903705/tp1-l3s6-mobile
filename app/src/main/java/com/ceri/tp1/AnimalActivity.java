package com.ceri.tp1;

import android.content.Intent;
import android.media.Image;
import android.support.annotation.DrawableRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.HashMap;

import fr.uavignon.shuet.tp1.data.Animal;
import fr.uavignon.shuet.tp1.data.AnimalList;

public class AnimalActivity extends AppCompatActivity
{
    private Animal animal_obj;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        String animal_name = getIntent().getExtras().getString("animal");

        AnimalList animalList = new AnimalList();
        animal_obj = AnimalList.getAnimal(animal_name);

        // ===============================
        //        ASSIGN ELEMENTS
        // ===============================

        ((TextView)findViewById(R.id.text_name)).setText(animal_name);
        ((ImageView)findViewById(R.id.image_animal)).setImageResource(getResources().getIdentifier(animal_obj.getImgFile(),"drawable", getPackageName()));

        ((TextView)findViewById(R.id.text_esperance_val)).setText(animal_obj.getStrHightestLifespan());
        ((TextView)findViewById(R.id.text_gestation_val)).setText(animal_obj.getStrGestationPeriod());
        ((TextView)findViewById(R.id.text_poids_naiss_val)).setText(animal_obj.getStrBirthWeight());
        ((TextView)findViewById(R.id.text_poids_adult_val)).setText(animal_obj.getStrAdultWeight());
        ((TextView)findViewById(R.id.field_conserv)).setText(animal_obj.getConservationStatus());

        // ===============================


    }

    public void save(View v)
    {
        String conserv = ((TextView)findViewById(R.id.field_conserv)).getText().toString();
        animal_obj.setConservationStatus(conserv);
    }
}
