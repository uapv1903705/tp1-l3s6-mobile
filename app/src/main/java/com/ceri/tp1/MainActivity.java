package com.ceri.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


import java.util.HashMap;

import fr.uavignon.shuet.tp1.data.Animal;
import fr.uavignon.shuet.tp1.data.AnimalList;

import static android.app.ProgressDialog.show;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ListView listView = findViewById(R.id.listView);

        AnimalList animalList = new AnimalList();

        String[] values = animalList.getNameArray();

        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                final String item = (String) parent.getItemAtPosition(position);
                Intent appInfo = new Intent(MainActivity.this, AnimalActivity.class);
                appInfo.putExtra("animal",item);
                startActivity(appInfo);
            }
        });
    }
}
